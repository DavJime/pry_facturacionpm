﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoData;
using LogicaNegocio;

namespace PryFacturacion.Formularios.Usuarios
{
    public partial class frm_usuario : Form
    {
        public frm_usuario()
        {
            InitializeComponent();
        }

        private void cargarRoles()
        {
            List<TBL_ROL> _listaRoles = new List<TBL_ROL>();
            _listaRoles = logicaRol.obtenerRoles();
            if (_listaRoles.Count > 0 && _listaRoles != null)
            {
                _listaRoles.Insert(0, new TBL_ROL { ROL_CODIGO = 0, ROL_DESCRIPCION = "Seleccione" });
                cmb_rol.DataSource = _listaRoles;
                cmb_rol.DisplayMember = "ROL_DESCRIPCION";
                cmb_rol.ValueMember = "ROL_CODIGO";
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void cargarUsuarios()
        {
            List<TBL_USUARIO> _lista = new List<TBL_USUARIO>();
            _lista = logicaUsuario.obtenerUsuarios();
            if (_lista.Count > 0 && _lista != null)
            {
                dgv_usuario.DataSource = _lista.Select(data => new
                {
                    //generar nuevo objeto para cambiar nombre a los encabezados
                    codigo = data.USU_CODIGO,
                    usuario = data.USU_EMAIL,
                    correo = data.USU_EMAIL,
                    rol = data.TBL_ROL.ROL_DESCRIPCION
                }).ToList();
            }
        }

        private void frm_usuario_Load(object sender, EventArgs e)
        {
            try
            {
                cargarUsuarios();
                cargarRoles();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error" + ex.Message, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void limpiarTexto()
        {
            lbl_codigo.Text = "";
            txt_correo.Clear();
            txt_clave.Clear();
            txt_usuario.Clear();
            cmb_rol.SelectedIndex = 0;
        }

        private void guardar()
        {
            //validar campos obligatorios
            TBL_USUARIO _infoUsuario = new TBL_USUARIO();
            _infoUsuario.USU_EMAIL = txt_correo.Text;
            _infoUsuario.USU_PASSWORD = LogicaNegocio.Complementos.cls_encriptar.EncriptarClaveMD5(txt_clave.Text);
            _infoUsuario.ROL_CODIGO = Convert.ToInt16(cmb_rol.SelectedValue.ToString());
            bool resInsert = logicaUsuario.saveUser(_infoUsuario);
            if (resInsert)
            {
                MessageBox.Show("Usuario Guardado Correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiarTexto();
                cargarUsuarios();
            }
        }

        private void actualizar()
        {
            ////validar campos obligatorios
            //TBL_USUARIO _infoUsuario = new TBL_USUARIO();
            //_infoUsuario.USU_EMAIL = txt_correo.Text;
            //_infoUsuario.USU_PASSWORD = LogicaNegocio.Complementos.cls_encriptar.EncriptarClaveMD5(txt_clave.Text);
            //_infoUsuario.ROL_CODIGO = Convert.ToInt16(cmb_rol.SelectedValue.ToString());
            //bool resInsert = logicaUsuario.saveUser(_infoUsuario);
            //if (resInsert)
            //{
            //    MessageBox.Show("Usuario Guardado Correctamente", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    limpiarTexto();
            //    cargarUsuarios();
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            limpiarTexto();
        }

        private void btn_guardar_Click(object sender, EventArgs e)
        {
            guardar();
        }

        private void dgv_usuario_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var codigoUsuario = dgv_usuario.Rows[e.RowIndex].Cells["codigo"].Value;
            var usuario = dgv_usuario.Rows[e.RowIndex].Cells["usuario"].Value;
            var correo = dgv_usuario.Rows[e.RowIndex].Cells["correo"].Value;
            var rol = dgv_usuario.Rows[e.RowIndex].Cells["rol"].Value;
            if (!string.IsNullOrEmpty(codigoUsuario.ToString()))
            {
                lbl_codigo.Text = codigoUsuario.ToString();
                txt_correo.Text = correo.ToString();
                txt_usuario.Text = usuario.ToString();
                cmb_rol.SelectedIndex = cmb_rol.FindString(rol.ToString());
                string password = logicaUsuario.obtenerPasswordXIdUsuario(Convert.ToInt16(codigoUsuario));
                txt_clave.Text = password;
            }
        }
    }
}
