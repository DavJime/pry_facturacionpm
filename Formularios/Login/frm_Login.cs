﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoData;
using LogicaNegocio;

namespace PryFacturacion.Formularios.Login
{
    public partial class frm_Login : Form
    {
        public frm_Login()
        {
            InitializeComponent();
        }

        private int count = 0;
        private string intentosUsuario = "";

        private void button2_Click(object sender, EventArgs e)
        {
            
            string mensaje = "";
            if (string.IsNullOrEmpty(txt_email.Text))
            {
                mensaje += "* Email campo ibligatorio\n";
            }
            if (string.IsNullOrEmpty(txt_password.Text))
            {
                mensaje += "* Clave campo obligatorio\n";
            }
            if (!string.IsNullOrEmpty(mensaje))
            {
                MessageBox.Show(mensaje,"Sistema",MessageBoxButtons.OK,MessageBoxIcon.Information);
                txt_email.Focus();
            }
            else
            {
                TBL_USUARIO _infoUser = new TBL_USUARIO();
                _infoUser = logicaUsuario.obtenerUsuariosXLogin(txt_email.Text,LogicaNegocio.Complementos.cls_encriptar.EncriptarClaveMD5( txt_password.Text));
                if (_infoUser != null)
                {

                    if (_infoUser.USU_STATUS == 'B')
                    {
                        MessageBox.Show("Su usuario esta Bloqueado" + _infoUser.TBL_ROL.ROL_DESCRIPCION, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Hide();
                        return;
                    }

                    MessageBox.Show("Bienvenido al Sistema: "+_infoUser.TBL_ROL.ROL_DESCRIPCION, "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    frm_Principal frmPrincipal = new frm_Principal();
                    frmPrincipal.Show();
                    this.Hide();
                }

                if (_infoUser == null)
                {
                    string usuario_email_intento = txt_email.Text;
                    if (intentosUsuario == "")
                    {
                        intentosUsuario = txt_email.Text;
                    }

                    if (usuario_email_intento == intentosUsuario)
                    {
                        count += 1;
                        intentosUsuario = txt_email.Text;
                        string iVu = txt_email.Text;
                        TBL_USUARIO _infoUserVeri = new TBL_USUARIO();
                        _infoUserVeri = logicaUsuario.obtenerUsuariosXUser(iVu);
                        if (_infoUserVeri != null)
                        {
                            MessageBox.Show("Clave ingresada no Valida", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Usuario:" + txt_email.Text + " no esta Registrado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }

                    if (usuario_email_intento != intentosUsuario)
                    {
                        count = 1;
                        intentosUsuario = txt_email.Text;
                        string iVu = txt_email.Text;
                        TBL_USUARIO _infoUserVeri = new TBL_USUARIO();
                        _infoUserVeri = logicaUsuario.obtenerUsuariosXUser(iVu);
                        if (_infoUserVeri != null)
                        {
                            MessageBox.Show("Clave ingresada no Valida", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("Usuario:" + txt_email.Text + " no esta Registrado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }

                if (count == 3)
                {
                    TBL_USUARIO res = logicaUsuario.obtenerUsuariosXUser(txt_email.Text);
                    if (res != null)
                    {
                        bool bloqueado = logicaUsuario.blockUser(res);
                        if (bloqueado)
                        {
                            MessageBox.Show("Usuario Bloqueado", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            this.Close();
                        }
                    }
                    if (res == null)
                    {
                        MessageBox.Show("Sistema Bloqueado comuniquese con TICs", "Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        this.Close();
                    }
                }
            }
        }

        private void botonCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frm_Login_Load(object sender, EventArgs e)
        {
        }
    }
}
