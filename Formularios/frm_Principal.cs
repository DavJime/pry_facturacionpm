﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoData;
using LogicaNegocio;
using PryFacturacion;

namespace PryFacturacion.Formularios
{
    public partial class frm_Principal : Form
    {
        public frm_Principal()
        {
            InitializeComponent();
            IsMdiContainer = true;
        }

        private void usuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formularios.Usuarios.frm_usuario _frmUsuario = new Usuarios.frm_usuario();
            _frmUsuario.MdiParent = this;
            _frmUsuario.StartPosition = FormStartPosition.CenterScreen;
            _frmUsuario.Show();




        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            tsl_hora.Text = DateTime.Now.ToLongTimeString();
            tsl_fecha.Text = DateTime.Now.ToLongDateString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {

        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
